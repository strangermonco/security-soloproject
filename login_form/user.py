from login_form.db import get_db  # Import the get_db function from the login_form.db module
from werkzeug.security import check_password_hash, generate_password_hash  # Import functions for password hashing
#These were delcared but weren't in use, as they were dimmed in colour. 


class User():
  @classmethod
  def create(cls, username, password):
    db = get_db()
    #Hash the password by using the imported security module
    hashed_password = generate_password_hash(password)
    #Insert user and hashed password into database instead of the normal password
    db.execute("INSERT INTO user (username, password) VALUES (?, ?)", (username, hashed_password))
    db.commit()

  @classmethod
  def find_with_credentials(cls, username, password):
    db = get_db()
    # Query the database for the user with the given username
    user = db.execute(
    "SELECT id, username, password FROM user WHERE username = ?", (username,)).fetchone()
    if user and check_password_hash(user['password'], password):
        return User(user['username'], user['password'], user['id'])
    else:
      return None 

  @classmethod
  def find_by_id(cls, user_id):
    user = get_db().execute(
      'SELECT id, username, password FROM user WHERE id = ?', (user_id,)
    ).fetchone()
    if user:
      return User(user['username'], user['password'], user['id'])
    else:
      return None  

  def __init__(self, username, password, id):
    self.username = username
    self.password = password
    self.id = id
